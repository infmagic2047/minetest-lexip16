textures = {
    'technic_akalin_dust': ('akalin', 'dust', 'dust'),
    'technic_alatro_dust': ('alatro', 'dust', 'dust'),
    'technic_arol_dust': ('arol', 'dust', 'dust'),
    'technic_brass_dust': ('brass', 'dust', 'dust'),
    'technic_bronze_dust': ('bronze', 'dust', 'dust'),
    'technic_carbon_steel_dust': ('carbon_steel', 'dust', 'dust'),
    'technic_cast_iron_dust': ('cast_iron', 'dust', 'dust'),
    'technic_chromium_dust': ('chromium', 'dust', 'dust'),
    'technic_coal_dust': ('coal', 'dust', 'dust'),
    'technic_copper_dust': ('copper', 'dust', 'dust'),
    'technic_gold_dust': ('gold', 'dust', 'dust'),
    'technic_kalite_dust': ('kalite', 'dust', 'dust'),
    'technic_lead_dust': ('lead', 'dust', 'dust'),
    'technic_mithril_dust': ('mithril', 'dust', 'dust'),
    'technic_rubber': ('rubber', 'circle', 'circle'),
    'technic_sawdust': ('wood', 'dust', 'dust'),
    'technic_silver_dust': ('silver', 'dust', 'dust'),
    'technic_stainless_steel_dust': ('stainless_steel', 'dust', 'dust'),
    'technic_sulfur_dust': ('sulfur', 'dust', 'dust'),
    'technic_talinite_dust': ('talinite', 'dust', 'dust'),
    'technic_tin_dust': ('tin', 'dust', 'dust'),
    'technic_uranium_dust': ('uranium', 'dust', 'dust'),
    'technic_wrought_iron_dust': ('steel', 'dust', 'dust'),
    'technic_zinc_dust': ('zinc', 'dust', 'dust'),
}
