textures = {
    'carts_rail_crossing': (('wood', 'steel'), 'rail_crossing', 'rail_crossing'),
    'carts_rail_curved': (('wood', 'steel'), 'rail_curved', 'rail_curved'),
    'carts_rail_straight': (('wood', 'steel'), 'rail', 'rail'),
    'carts_rail_t_junction': (('wood', 'steel'), 'rail_t_junction', 'rail_t_junction'),
}
